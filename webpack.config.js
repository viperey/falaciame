const path = require('path');
// If you are using mini-css-extract-plugin
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: {
    bundle: ['./content.js', './config.js'],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [
          // For style-loader
          'style-loader', 
          // For mini-css-extract-plugin
          MiniCssExtractPlugin.loader, 
          'css-loader',
        ],
      },
      // your existing rules
    ],
  },
  plugins: [
    // If you are using mini-css-extract-plugin
    new MiniCssExtractPlugin(),
  ],
  mode: 'development'
};
