# Falaciame

Falaciame es una extensión de navegador que analiza los comentarios en la plataforma de una red social y proporciona información y evaluaciones. Utiliza el poder de la inteligencia artificial para generar informes de análisis de comentarios utilizando la API ChatGPT de OpenAI.

## Introducción

En la plataforma de la red social Meneame, los comentarios desempeñan un papel importante en las conversaciones y debates. Sin embargo, la calidad de los comentarios puede variar ampliamente, y comprender su valor y relevancia puede resultar desafiante.

La extensión Falaciame ha sido desarrollada para abordar este desafío al brindar una evaluación objetiva y detallada de los comentarios. Utilizando la tecnología de inteligencia artificial, la extensión genera informes de análisis que ayudan a comprender la construcción del comentario, su toxicidad y la presencia de posibles falacias.

Al utilizar Falaciame, podrás obtener una visión más profunda de los comentarios en Meneame, lo que te permitirá identificar aquellos que son constructivos, evaluar su calidad y entender mejor las diversas perspectivas presentes en la comunidad. Esta herramienta te ayudará a tomar decisiones informadas sobre la relevancia y confiabilidad de los comentarios en tus interacciones en la plataforma.

## Installation

1. Clone the repository:
   ```shell
   git clone https://github.com/your-username/comment-analyzer.git
   ```

2. Install the required dependencies:
   ```shell
   npm install
   ```

3. Create the configuration file:
   - Create a file named `config.js` in the project root directory.
   - Add your OpenAI API key to the `config.js` file:
     ```javascript
     // config.js
     const apiKey = 'YOUR_API_KEY_HERE';
     export default apiKey;
     ```
   - Make sure to keep the `config.js` file secure and do not expose your API key publicly.

4. Build the project:
   ```shell
   npm run build
   ```

## Usage

1. Open your Firefox browser.

2. Add the extension to Firefox:
   - Type `about:debugging` in the Firefox address bar.
   - Click on "This Firefox" on the left sidebar.
   - Click on "Load Temporary Add-on..." button.
   - Select the `manifest.json` file from the `dist` directory in the cloned repository.

3. Visit the social network platform where you want to analyze comments.

4. Interact with the comments:
   - Click on the ChatGPT icon next to a comment to analyze it.
   - The extension will provide an evaluation report based on the comment's constructiveness, toxicity, fallacies, and more.

## Contributing

Contributions are welcome! If you find any issues or have suggestions for improvements, please open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).
```
