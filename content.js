const marked = require('marked');
import './main.css';

const apikey = require('./config.js');

console.log(apikey);

// console.log('beginning');
if (document.readyState === 'loading') {
    // console.log('loading');
    document.addEventListener('DOMContentLoaded', myFunction);
} else {
    // console.log('done');
    myFunction();
}

function myFunction(){
    // Query all GitLab MR comment elements
    let commentElements = document.querySelectorAll('.comment');

    commentElements.forEach(comment => {
        let commentText = comment.querySelector('.comment-text');
        let commentTextStr = commentText.textContent
        let gptAnchor = createButton(commentTextStr, comment, false);
        let commentFooter = comment.querySelector('.comment-footer');
        commentFooter.insertBefore(gptAnchor, commentFooter.firstChild);
    });
}

function createButton(combinedText, comment, useFakeAPI) {
    // Create the anchor and the icon
    let newAnchor = document.createElement('a');
    newAnchor.href = 'javascript:void(0);';
    newAnchor.title = 'Analyze with ChatGPT';
    newAnchor.style.position = 'relative'; // To match the style of other anchors

    let newIcon = document.createElement('i');
    newIcon.className = 'fa fa-question-circle'; // You might need to adjust this depending on the icon set you are using

    newAnchor.appendChild(newIcon);

    newAnchor.addEventListener('click', function() {
        let uniqueId = generateUniqueId(); // Replace this with your own unique identifier generation logic
        addGTPCommentAnswerBox(comment, uniqueId);
        if (useFakeAPI) {
            // Simulate the API request with a delay
            simulateGPTAPICall(combinedText)
                .then(response => {
                    console.log('Fake API response:', response);
                    injectCommentAnalysisResponse(uniqueId, response.result);
                })
                .catch(error => {
                    console.error('Error:', error);
                });
        } else {
            makeActualAPIRequest(combinedText, comment, uniqueId);
        }
    });

    return newAnchor;
}

function addGTPCommentAnswerBox(originalComment, uniqueId) {
    let commentAnswersButton = originalComment.querySelector('a.comment-answers');
    if (commentAnswersButton) {
        commentAnswersButton.click();
        let fakeReplyAdded = false; // Flag to track whether the fake reply has been added

        const observer = new MutationObserver((mutationsList) => {
            for (let mutation of mutationsList) {
                if (mutation.type === 'childList') {
                    let parentCommentLi = originalComment.parentNode;
                    let commentAnswersDiv = parentCommentLi.querySelector('div.comment-answers');

                    if (!commentAnswersDiv) {
                        // If the 'comment-answers' div does not exist, create it
                        commentAnswersDiv = document.createElement('div');
                        commentAnswersDiv.className = 'comment-answers';
                        parentCommentLi.appendChild(commentAnswersDiv);
                    }

                    if (!fakeReplyAdded) {
                        // Create the new reply element
                        let fakeReply = document.createElement('div');
                        fakeReply.className = 'comment';

                        fakeReply.innerHTML = `
                            <div class="comment-body">
                                <div class="comment-header">
                                    <a href="javascript:void(0);" class="username">ChatGPT</a>
                                </div>
                                <div class="comment-text fade-animation" id="${uniqueId}">
                                    ${getRandomChoice()}
                                </div>
                            </div>
                        `;

                        // Append the new reply to the 'comment-answers' div
                        commentAnswersDiv.appendChild(fakeReply);
                        console.log('Creating fake reply');

                        fakeReplyAdded = true; // Set the flag to true indicating the fake reply has been added
                    }
                }
            }
        });

        const config = { childList: true, subtree: true };
        observer.observe(document, config);
    } else {
        let parentCommentLi = originalComment.parentNode;
        let commentAnswersDiv = parentCommentLi.querySelector('div.comment-answers');
        if (!commentAnswersDiv) {
            commentAnswersDiv = document.createElement('div');
            commentAnswersDiv.className = 'comment-answers';
            commentAnswersDiv.style.display = 'block';
            parentCommentLi.appendChild(commentAnswersDiv);
        }

        // Create the new reply element
        let fakeReply = document.createElement('div');
        fakeReply.className = 'comment';

        fakeReply.innerHTML = `
        <div class="comment-body">
        <div class="comment-header">
        <a href="javascript:void(0);" class="username">ChatGPT</a>
        </div>
        <div class="comment-text fade-animation" id="${uniqueId}">
        ${getRandomChoice()}
        </div>
        </div>
        `;

        // Append the new reply to the 'comment-answers' div
        commentAnswersDiv.appendChild(fakeReply);
        console.log('Creating fake reply');
    }
}

function generateUniqueId() {
    return 'api-response-id-' + Math.random().toString(36).substr(2, 9);
}

function makeActualAPIRequest(combinedText, comment, uniqueId) {
    fetch('https://api.openai.com/v1/chat/completions', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + apikey
        },
        body: JSON.stringify({
            'model': 'gpt-3.5-turbo',
            'messages': [
                {
                    "role": "system",
                    "content": `You are a helpful assistant that scan comments in a news social network similar to Reddit called Meneame.net. Your goal is to emit reports over the quality of the comments.
                        Follow this rules of evaluation:
                        + Evaluate its constructiveness on a scale from 0 to 10.
                        + Evaluate its toxicity on a scale from 0 to 10.
                        + Evaluate how fallacious it is on a scale from 0 to 10. Include a subsection of up to 3 potential fallacies, rate on percent scale. If percent is below 50% stop including.
                        + Provide a counter option that would be a more constructive way to give the same feedback.
                        + Make the evaluation terms bold. Add a small comment after each term that reasons the score.
                        + Use a non-numeric bullet points format. Use ul/li tags.
                        + Use html format for style. Also new lines should be in html tags.
                        + Use a "<b>{term} ({score}%):</b> {term_comment}" format.
                        + Reply in spanish.
                        + The response ul may have transparent background.
                        + Provide a final score value based on the ones given to the main terms. Use percentage scale.
                    `
                },
                {
                    "role": "user",
                    "content": `Evaluate the following comment according to the instructions given: '${combinedText}'`
                }
            ],
            'max_tokens': 2000,
            "temperature": 1.0,
        })
    })
    .then(response => response.json())
    .then(data => {
        console.log('GPT-3 response:', data);
        let gpt3Response = data.choices[0].message.content;  // Extract the response text
        injectCommentAnalysisResponse(uniqueId, gpt3Response);
    })
    .catch(error => {
        console.error('Error:', error);
    });
}

function injectCommentAnalysisResponse(uniqueId, text) {
    const divElement = document.getElementById(uniqueId);
    divElement.classList.remove('fade-animation');
    if (divElement) {
        divElement.innerHTML = text;
    }
}

function simulateGPTAPICall(request) {
  return new Promise((resolve, reject) => {
    // Simulate a 1-second delay using setTimeout
    setTimeout(() => {
      // Mock response data
      const mockResponse = {
        result: "This is a mock response from the GPT API",
        request: request
      };
      
      // Resolve the promise with the mock response
      resolve(mockResponse);
    }, 1000); // 1-second delay
  });
}

function getRandomChoice() {
  const phrases = [
    "El oráculo está pensando...",
    "Estoy procesando la información...",
    "Analizando alta turra, esto puede llevar un rato",
    "Madre de Dios que cantidad de sandeces. Me llevará un rato responder...",
    "Analizando los datos...",
    "Buscando la respuesta definitiva...",
    "Reflexionando sobre el comentario...",
    "Meneando mis neuronas...",
    "Descifrando los misterios del texto...",
    "Haciendo mi mejor esfuerzo...",
    "Consultando la sabiduría del universo...",
    "En búsqueda de la noticia más meneada...",
    "Siguiendo el estilo Gallir...",
    "¡Meneame Premium: La respuesta más exclusiva!",
    "Leyendo entre líneas como todo buen meneante...",
    "Conectando con el espíritu de Menéame...",
    "Descubriendo el hilo de comentarios más interesante...",
    "Desenmascarando fake news...",
    "Enviando una señal al servidor de Menéame...",
    "Sumergiéndome en el karma de la comunidad...",
    "Sintonizando con el debate meneante...",
    "Análisis profundo en curso...",
    "Buscando la verdad entre tanta cháchara...",
    "Aplicando la fórmula meneante secreta...",
    "Girando las ruedas del pensamiento meneamero...",
    "Cavilando sobre la vida y los memes...",
    "Descifrando los enigmas meneantes...",
    "Sopesando cada palabra con cuidado...",
    "Encontrando el equilibrio entre ingenio y sarcasmo...",
    "Desplegando el poder del karma en la respuesta...",
    "Navegando por los mares de la información...",
    "Sumergiéndome en el océano de comentarios...",
  ];

  const randomIndex = Math.floor(Math.random() * phrases.length);
  return phrases[randomIndex];
}
